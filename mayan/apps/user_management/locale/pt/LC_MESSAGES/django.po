# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Manuela Silva <inactive+h_manuela_rodsilva@transifex.com>, 2015
# Renata Oliveira <renatabels@gmail.com>, 2011
# Roberto Rosario, 2012
# Vítor Figueiró <vfigueiro@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:33-0400\n"
"PO-Revision-Date: 2018-09-27 02:32+0000\n"
"Last-Translator: Vítor Figueiró <vfigueiro@gmail.com>\n"
"Language-Team: Portuguese (http://www.transifex.com/rosarior/mayan-edms/language/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:53 permissions.py:7
msgid "User management"
msgstr "Gestão de utilizadores"

#: apps.py:68
msgid "All the groups."
msgstr "Todos os grupos."

#: apps.py:72
msgid "All the users."
msgstr "Todos os utilziadores."

#: apps.py:90 links.py:33 links.py:57 links.py:78 views.py:274
msgid "Users"
msgstr "Utilizadores"

#: apps.py:94
msgid "Full name"
msgstr "Nome completo"

#: apps.py:97 search.py:22
msgid "Email"
msgstr "Correio eletrónico"

#: apps.py:100
msgid "Active"
msgstr "Ativo"

#: apps.py:106
msgid "Has usable password?"
msgstr ""

#: links.py:18 views.py:32
msgid "Create new group"
msgstr "Criar novo grupo"

#: links.py:22 links.py:46 links.py:61
msgid "Delete"
msgstr "Eliminar"

#: links.py:25 links.py:49
msgid "Edit"
msgstr "Editar"

#: links.py:29 links.py:38 links.py:53 search.py:25 views.py:71
msgid "Groups"
msgstr "Grupos"

#: links.py:42 views.py:135
msgid "Create new user"
msgstr "Criar novo utilziador"

#: links.py:65 links.py:74
msgid "Set password"
msgstr ""

#: links.py:70
msgid "User options"
msgstr ""

#: models.py:13
msgid "User"
msgstr "Utilizador"

#: models.py:17
msgid "Forbid this user from changing their password."
msgstr ""

#: models.py:23
msgid "User settings"
msgstr ""

#: models.py:24
msgid "Users settings"
msgstr ""

#: permissions.py:10
msgid "Create new groups"
msgstr "Criar novos grupos"

#: permissions.py:13
msgid "Delete existing groups"
msgstr "Eliminar grupos existentes"

#: permissions.py:16
msgid "Edit existing groups"
msgstr "Editar grupos existentes"

#: permissions.py:19
msgid "View existing groups"
msgstr "Ver grupos existentes"

#: permissions.py:22
msgid "Create new users"
msgstr "Criar novos utilizadores"

#: permissions.py:25
msgid "Delete existing users"
msgstr "Eliminar utilizadores existentes"

#: permissions.py:28
msgid "Edit existing users"
msgstr "Editar utilizadores existentes"

#: permissions.py:31
msgid "View existing users"
msgstr "Ver utilizadores existentes"

#: search.py:19
msgid "First name"
msgstr ""

#: search.py:28
msgid "Last name"
msgstr ""

#: search.py:31
msgid "username"
msgstr ""

#: search.py:41
msgid "Name"
msgstr "Nome"

#: serializers.py:34
msgid "Comma separated list of group primary keys to assign this user to."
msgstr ""

#: serializers.py:64
msgid "List of group primary keys to which to add the user."
msgstr ""

#: views.py:48
#, python-format
msgid "Edit group: %s"
msgstr "Editar grupo: %s"

#: views.py:64
msgid ""
"User groups are organizational units. They should mirror the organizational "
"units of your organization. Groups can't be used for access control. Use "
"roles for permissions and access control, add groups to them."
msgstr ""

#: views.py:70
msgid "There are no user groups"
msgstr ""

#: views.py:83
#, python-format
msgid "Delete the group: %s?"
msgstr "Eliminar o grupo: %s?"

#: views.py:89
msgid "Available users"
msgstr ""

#: views.py:90
msgid "Users in group"
msgstr ""

#: views.py:111
#, python-format
msgid "Users of group: %s"
msgstr ""

#: views.py:145
#, python-format
msgid "User \"%s\" created successfully."
msgstr "Utilizador \"%s\" criado com sucesso."

#: views.py:157
#, python-format
msgid "User delete request performed on %(count)d user"
msgstr ""

#: views.py:159
#, python-format
msgid "User delete request performed on %(count)d users"
msgstr ""

#: views.py:167
msgid "Delete user"
msgid_plural "Delete users"
msgstr[0] ""
msgstr[1] ""

#: views.py:177
#, python-format
msgid "Delete user: %s"
msgstr ""

#: views.py:189
msgid ""
"Super user and staff user deleting is not allowed, use the admin interface "
"for these cases."
msgstr "Não é permitida a eliminação de administradores ou membros da equipa, utilize a interface de administrador para estes casos."

#: views.py:197
#, python-format
msgid "User \"%s\" deleted successfully."
msgstr "Utilizador \"%s\" eliminado com sucesso."

#: views.py:203
#, python-format
msgid "Error deleting user \"%(user)s\": %(error)s"
msgstr "Erro ao eliminar o utilizador \"%(user)s\": %(error)s "

#: views.py:219
#, python-format
msgid "Edit user: %s"
msgstr "Editar utilizador: %s"

#: views.py:225
msgid "Available groups"
msgstr "Grupos disponíveis"

#: views.py:226
msgid "Groups joined"
msgstr ""

#: views.py:235
#, python-format
msgid "Groups of user: %s"
msgstr "Grupos do utilizador: %s"

#: views.py:270
msgid ""
"User accounts can be create from this view. After creating an user account "
"you will prompted to set a password for it. "
msgstr ""

#: views.py:273
msgid "There are no user accounts"
msgstr ""

#: views.py:290
#, python-format
msgid "Edit options for user: %s"
msgstr ""

#: views.py:312
#, python-format
msgid "Password change request performed on %(count)d user"
msgstr ""

#: views.py:314
#, python-format
msgid "Password change request performed on %(count)d users"
msgstr ""

#: views.py:321
msgid "Submit"
msgstr "Submeter"

#: views.py:323
msgid "Change user password"
msgid_plural "Change users passwords"
msgstr[0] ""
msgstr[1] ""

#: views.py:333
#, python-format
msgid "Change password for user: %s"
msgstr ""

#: views.py:354
msgid ""
"Super user and staff user password reseting is not allowed, use the admin "
"interface for these cases."
msgstr "Não é permitido redefinir a senha de administradores ou de membros da equipa, utilize a interface de administrador para estes casos."

#: views.py:364
#, python-format
msgid "Successful password reset for user: %s."
msgstr ""

#: views.py:370
#, python-format
msgid "Error reseting password for user \"%(user)s\": %(error)s"
msgstr "Erro ao redefinir a senha para o utilizador \"%(user)s\": %(error)s "
