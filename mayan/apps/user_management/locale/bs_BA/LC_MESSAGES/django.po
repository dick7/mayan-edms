# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Atdhe Tabaku <Atdhe617@gmail.com>, 2018
# www.ping.ba <jomer@ping.ba>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:33-0400\n"
"PO-Revision-Date: 2018-09-27 02:32+0000\n"
"Last-Translator: www.ping.ba <jomer@ping.ba>\n"
"Language-Team: Bosnian (Bosnia and Herzegovina) (http://www.transifex.com/rosarior/mayan-edms/language/bs_BA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs_BA\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: apps.py:53 permissions.py:7
msgid "User management"
msgstr "Upravljanje korisnicima"

#: apps.py:68
msgid "All the groups."
msgstr "Sve grupe."

#: apps.py:72
msgid "All the users."
msgstr "Svi korisnici."

#: apps.py:90 links.py:33 links.py:57 links.py:78 views.py:274
msgid "Users"
msgstr "Korisnici"

#: apps.py:94
msgid "Full name"
msgstr "Ime i prezime"

#: apps.py:97 search.py:22
msgid "Email"
msgstr "Email"

#: apps.py:100
msgid "Active"
msgstr "Aktivan"

#: apps.py:106
msgid "Has usable password?"
msgstr "Ima korisnu lozinku?"

#: links.py:18 views.py:32
msgid "Create new group"
msgstr "Kreiraj novu grupu"

#: links.py:22 links.py:46 links.py:61
msgid "Delete"
msgstr "Obriši"

#: links.py:25 links.py:49
msgid "Edit"
msgstr "Urediti"

#: links.py:29 links.py:38 links.py:53 search.py:25 views.py:71
msgid "Groups"
msgstr "Grupe"

#: links.py:42 views.py:135
msgid "Create new user"
msgstr "Kreirajte novog korisnika"

#: links.py:65 links.py:74
msgid "Set password"
msgstr "Postavite lozinku"

#: links.py:70
msgid "User options"
msgstr ""

#: models.py:13
msgid "User"
msgstr "Korisnik"

#: models.py:17
msgid "Forbid this user from changing their password."
msgstr ""

#: models.py:23
msgid "User settings"
msgstr ""

#: models.py:24
msgid "Users settings"
msgstr ""

#: permissions.py:10
msgid "Create new groups"
msgstr "Kreiraj nove grupe"

#: permissions.py:13
msgid "Delete existing groups"
msgstr "Obriši postojeće grupe"

#: permissions.py:16
msgid "Edit existing groups"
msgstr "Izmjeni postojeće grupe"

#: permissions.py:19
msgid "View existing groups"
msgstr "Pregledaj postojeće grupe"

#: permissions.py:22
msgid "Create new users"
msgstr "Kreiraj nove korisnike"

#: permissions.py:25
msgid "Delete existing users"
msgstr "Obriši postojeće korisnike"

#: permissions.py:28
msgid "Edit existing users"
msgstr "Izmjeni postojeće korisnike"

#: permissions.py:31
msgid "View existing users"
msgstr "Pregledaj postojeće korisnike"

#: search.py:19
msgid "First name"
msgstr "Ime"

#: search.py:28
msgid "Last name"
msgstr "Prezime"

#: search.py:31
msgid "username"
msgstr "korisnik"

#: search.py:41
msgid "Name"
msgstr "Ime"

#: serializers.py:34
msgid "Comma separated list of group primary keys to assign this user to."
msgstr "Lista odvojenih grupisanih primarnih ključeva za određivanje ovog korisnika."

#: serializers.py:64
msgid "List of group primary keys to which to add the user."
msgstr "Lista primarnih ključeva grupe kojima dodate korisnika."

#: views.py:48
#, python-format
msgid "Edit group: %s"
msgstr "Izmeni grupu: %s"

#: views.py:64
msgid ""
"User groups are organizational units. They should mirror the organizational "
"units of your organization. Groups can't be used for access control. Use "
"roles for permissions and access control, add groups to them."
msgstr ""

#: views.py:70
msgid "There are no user groups"
msgstr ""

#: views.py:83
#, python-format
msgid "Delete the group: %s?"
msgstr "Irbišite grupu: %s?"

#: views.py:89
msgid "Available users"
msgstr "Dostupni korisnici"

#: views.py:90
msgid "Users in group"
msgstr "Korisnici u grupi"

#: views.py:111
#, python-format
msgid "Users of group: %s"
msgstr "Korisnici u grupi: %s"

#: views.py:145
#, python-format
msgid "User \"%s\" created successfully."
msgstr "Korisnik \"%s\" uspješno kreiran."

#: views.py:157
#, python-format
msgid "User delete request performed on %(count)d user"
msgstr "Zahtev za brisanje korisnika je izvršen %(count)d korisnik"

#: views.py:159
#, python-format
msgid "User delete request performed on %(count)d users"
msgstr "Zahtev za brisanje korisnika je izvršen %(count)d korisnik"

#: views.py:167
msgid "Delete user"
msgid_plural "Delete users"
msgstr[0] "Izbrišite korisnika"
msgstr[1] "Izbrišite korisnike"
msgstr[2] "Izbrišite korisnike"

#: views.py:177
#, python-format
msgid "Delete user: %s"
msgstr "Izbrisati korisnike: %s"

#: views.py:189
msgid ""
"Super user and staff user deleting is not allowed, use the admin interface "
"for these cases."
msgstr "Super user i staff user brisanja nisu dozvoljena, koristite administratorski interface za takve slučajeve"

#: views.py:197
#, python-format
msgid "User \"%s\" deleted successfully."
msgstr "Korisnik \"%s\" uspješno obrisan."

#: views.py:203
#, python-format
msgid "Error deleting user \"%(user)s\": %(error)s"
msgstr "Greška brisanja korisnika \"%(user)s\": %(error)s"

#: views.py:219
#, python-format
msgid "Edit user: %s"
msgstr "Uređivanje korisnika: %s"

#: views.py:225
msgid "Available groups"
msgstr "Dostupne grupe"

#: views.py:226
msgid "Groups joined"
msgstr "Grupe su se pridružile"

#: views.py:235
#, python-format
msgid "Groups of user: %s"
msgstr "Grupe korisnika: %s"

#: views.py:270
msgid ""
"User accounts can be create from this view. After creating an user account "
"you will prompted to set a password for it. "
msgstr ""

#: views.py:273
msgid "There are no user accounts"
msgstr ""

#: views.py:290
#, python-format
msgid "Edit options for user: %s"
msgstr ""

#: views.py:312
#, python-format
msgid "Password change request performed on %(count)d user"
msgstr "Zahtev za promjenu lozinke izvršen %(count)d korisnika"

#: views.py:314
#, python-format
msgid "Password change request performed on %(count)d users"
msgstr "Zahtev za promjenu lozinke izvršen %(count)d korisnika"

#: views.py:321
msgid "Submit"
msgstr "Podnijeti"

#: views.py:323
msgid "Change user password"
msgid_plural "Change users passwords"
msgstr[0] "Promenite korisničku lozinku"
msgstr[1] "Promeni korisničke lozinke"
msgstr[2] "Promeni korisničke lozinke"

#: views.py:333
#, python-format
msgid "Change password for user: %s"
msgstr "Izmjenite lozinku korisnika: %s"

#: views.py:354
msgid ""
"Super user and staff user password reseting is not allowed, use the admin "
"interface for these cases."
msgstr "Super user i staff user reset lozinke nije dozvoljen, koristite administratorski interface za takve slučajeve"

#: views.py:364
#, python-format
msgid "Successful password reset for user: %s."
msgstr "Uspešno resetovanje lozinke za korisnika: %s"

#: views.py:370
#, python-format
msgid "Error reseting password for user \"%(user)s\": %(error)s"
msgstr "Greška resetovanja lozinke za korisnika \"%(user)s\": %(error)s"
