# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# lilo.panic, 2016
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:30-0400\n"
"PO-Revision-Date: 2018-09-27 02:30+0000\n"
"Last-Translator: Sergey Glita <gsv70@mail.ru>\n"
"Language-Team: Russian (http://www.transifex.com/rosarior/mayan-edms/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: apps.py:115 apps.py:268 events.py:7 menus.py:10 models.py:238
#: permissions.py:7 queues.py:18 settings.py:12 statistics.py:231
msgid "Documents"
msgstr "Документы"

#: apps.py:136
msgid "Create a document type"
msgstr "Создать тип документа"

#: apps.py:138
msgid ""
"Every uploaded document must be assigned a document type, it is the basic "
"way Mayan EDMS categorizes documents."
msgstr "Каждому загруженому документу должен быть присвоен тип документа, - это основной способ, которым Mayan EDMS  распределяет документы по категориям."

#: apps.py:157
msgid "Versions comment"
msgstr ""

#: apps.py:160
msgid "Versions encoding"
msgstr ""

#: apps.py:163
msgid "Versions mime type"
msgstr ""

#: apps.py:166
msgid "Versions timestamp"
msgstr ""

#: apps.py:231 apps.py:248 apps.py:255 apps.py:283 apps.py:298 apps.py:324
msgid "Thumbnail"
msgstr "Миниатюра"

#: apps.py:240 apps.py:307 forms.py:186 links.py:84
msgid "Pages"
msgstr "Страницы"

#: apps.py:262
msgid "Type"
msgstr "Тип"

#: apps.py:275 models.py:769
msgid "Enabled"
msgstr "Доступно"

#: apps.py:330 links.py:366 views/document_views.py:846
msgid "Duplicates"
msgstr ""

#: dashboard_widgets.py:24
msgid "Total pages"
msgstr ""

#: dashboard_widgets.py:46
msgid "Total documents"
msgstr ""

#: dashboard_widgets.py:65 views/document_views.py:168
msgid "Documents in trash"
msgstr "Документы в корзине"

#: dashboard_widgets.py:84 links.py:352 links.py:357 permissions.py:55
#: views/document_type_views.py:71
msgid "Document types"
msgstr "Типы документов"

#: dashboard_widgets.py:103
msgid "New documents this month"
msgstr ""

#: dashboard_widgets.py:116
msgid "New pages this month"
msgstr ""

#: events.py:10
msgid "Document created"
msgstr "Документ создан"

#: events.py:13
msgid "Document downloaded"
msgstr "Документ загружен"

#: events.py:16
msgid "New version uploaded"
msgstr "Новая версия была загружена"

#: events.py:19
msgid "Document properties edited"
msgstr "Свойства документа были изменены"

#: events.py:23
msgid "Document type changed"
msgstr "Тип документа был изменен"

#: events.py:27
msgid "Document type created"
msgstr ""

#: events.py:31
msgid "Document type edited"
msgstr ""

#: events.py:34
msgid "Document version reverted"
msgstr "Версия документа была восстановлена"

#: events.py:37
msgid "Document viewed"
msgstr "Документ просмотрен"

#: forms.py:96
msgid "Quick document rename"
msgstr "Быстро переименовать документ"

#: forms.py:104 forms.py:256
msgid "Preserve extension"
msgstr ""

#: forms.py:106
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open document correctly."
msgstr ""

#: forms.py:149
msgid "Date added"
msgstr "Дата добавления"

#: forms.py:153 models.py:185
msgid "UUID"
msgstr "UUID"

#: forms.py:155 models.py:209
msgid "Language"
msgstr "Язык"

#: forms.py:157
msgid "Unknown"
msgstr "Неизвестно"

#: forms.py:165
msgid "File mimetype"
msgstr "Mime тип файла"

#: forms.py:166 forms.py:171
msgid "None"
msgstr "Ни один"

#: forms.py:169
msgid "File encoding"
msgstr "Кодировка файла"

#: forms.py:175 models.py:1018
msgid "File size"
msgstr "Размер"

#: forms.py:180
msgid "Exists in storage"
msgstr "Существует в хранилище"

#: forms.py:182
msgid "File path in storage"
msgstr "Путь к файлу в хранилище"

#: forms.py:185 models.py:464 search.py:24 search.py:48
msgid "Checksum"
msgstr "Контрольная сумма"

#: forms.py:213 models.py:103 models.py:189 models.py:764 search.py:16
#: search.py:35
msgid "Document type"
msgstr "Тип документа"

#: forms.py:229
msgid "Compress"
msgstr "Сжать"

#: forms.py:231
msgid ""
"Download the document in the original format or in a compressed manner. This"
" option is selectable only when downloading one document, for multiple "
"documents, the bundle will always be downloads as a compressed file."
msgstr "Скачать документ в исходном, или в сжатом формате. Этот вариант доступен только при загрузке одного документа, для нескольких документов будет использован сжатый файл."

#: forms.py:238
msgid "Compressed filename"
msgstr "Имя сжатого файла"

#: forms.py:241
msgid ""
"The filename of the compressed file that will contain the documents to be "
"downloaded, if the previous option is selected."
msgstr "Имя файла сжатого файла, который будет содержать загружаемые документы, если выбран предыдущий параметр."

#: forms.py:258
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open the downloaded "
"document version correctly."
msgstr ""

#: forms.py:270 literals.py:39
msgid "Page range"
msgstr "Диапазон страниц"

#: forms.py:276
msgid ""
"Page number from which all the transformation will be cloned. Existing "
"transformations will be lost."
msgstr ""

#: links.py:70
msgid "Preview"
msgstr "Предварительный просмотр"

#: links.py:75
msgid "Properties"
msgstr "Свойства"

#: links.py:80 links.py:200
msgid "Versions"
msgstr "Версии"

#: links.py:92 links.py:152
msgid "Clear transformations"
msgstr "Очистить преобразования"

#: links.py:97
msgid "Clone transformations"
msgstr ""

#: links.py:102 links.py:160 links.py:325 links.py:340
msgid "Delete"
msgstr "Удалить"

#: links.py:106 links.py:164
msgid "Add to favorites"
msgstr ""

#: links.py:111 links.py:168
msgid "Remove from favorites"
msgstr ""

#: links.py:116 links.py:156
msgid "Move to trash"
msgstr "Переместить в корзину"

#: links.py:122
msgid "Edit properties"
msgstr "Редактировать свойства"

#: links.py:126 links.py:172
msgid "Change type"
msgstr "Изменить тип"

#: links.py:131 links.py:176
msgid "Advanced download"
msgstr ""

#: links.py:135
msgid "Print"
msgstr "Печать"

#: links.py:139
msgid "Quick download"
msgstr ""

#: links.py:143 links.py:179
msgid "Recalculate page count"
msgstr "Пересчитать количество страниц"

#: links.py:147 links.py:183
msgid "Restore"
msgstr "Восстановить"

#: links.py:189
msgid "Download version"
msgstr "Скачать версию"

#: links.py:194 links.py:275 models.py:237 models.py:427 models.py:1052
#: models.py:1082 models.py:1111
msgid "Document"
msgstr "Документ"

#: links.py:205
msgid "Details"
msgstr "Детали"

#: links.py:210 views/document_views.py:96
msgid "All documents"
msgstr "Все документы"

#: links.py:214 views/document_views.py:885
msgid "Favorites"
msgstr ""

#: links.py:218 views/document_views.py:969
msgid "Recently accessed"
msgstr ""

#: links.py:222 views/document_views.py:993
msgid "Recently added"
msgstr ""

#: links.py:226
msgid "Trash can"
msgstr ""

#: links.py:234
msgid ""
"Clear the graphics representations used to speed up the documents' display "
"and interactive transformations results."
msgstr "Очистить графику для ускорения отображения документов и интерактивных преобразований."

#: links.py:237
msgid "Clear document image cache"
msgstr "Очистить кэш изображений документа"

#: links.py:241 permissions.py:51
msgid "Empty trash"
msgstr "Очистить корзину"

#: links.py:250
msgid "First page"
msgstr "Первая страница"

#: links.py:255
msgid "Last page"
msgstr "Последняя страница"

#: links.py:263
msgid "Previous page"
msgstr "Предыдущая страница"

#: links.py:269
msgid "Next page"
msgstr "Следующая страница"

#: links.py:281
msgid "Rotate left"
msgstr "Повернуть влево"

#: links.py:286
msgid "Rotate right"
msgstr "Повернуть вправо"

#: links.py:289
msgid "Page image"
msgstr "Изображение страницы"

#: links.py:293
msgid "Reset view"
msgstr "Вернуть вид"

#: links.py:299
msgid "Zoom in"
msgstr "Увеличить"

#: links.py:305
msgid "Zoom out"
msgstr "Уменьшить"

#: links.py:313
msgid "Revert"
msgstr "Возврат"

#: links.py:320 views/document_type_views.py:86
msgid "Create document type"
msgstr "Создать тип документа"

#: links.py:329 links.py:345
msgid "Edit"
msgstr "Редактировать"

#: links.py:335
msgid "Add quick label to document type"
msgstr "Добавить быструю метку к документу"

#: links.py:349 models.py:775
msgid "Quick labels"
msgstr "Быстрые метки"

#: links.py:361 models.py:1055 models.py:1065 views/document_views.py:865
msgid "Duplicated documents"
msgstr ""

#: links.py:372
msgid "Duplicated document scan"
msgstr ""

#: literals.py:30
msgid "Default"
msgstr "Умолчание"

#: literals.py:39
msgid "All pages"
msgstr "Все страницы"

#: models.py:74
msgid "The name of the document type."
msgstr ""

#: models.py:75 models.py:193 models.py:767 search.py:21 search.py:42
msgid "Label"
msgstr "Надпись"

#: models.py:79
msgid ""
"Amount of time after which documents of this type will be moved to the "
"trash."
msgstr "Сколько должно пройти времени, прежде чем документы этого типа будут перемещены в корзину."

#: models.py:81
msgid "Trash time period"
msgstr "Период жизни"

#: models.py:85
msgid "Trash time unit"
msgstr "Единица измерения периода жизни"

#: models.py:89
msgid ""
"Amount of time after which documents of this type in the trash will be "
"deleted."
msgstr "Сколько должно пройти времени, прежде чем документы этого типа будут удалены из корзины."

#: models.py:91
msgid "Delete time period"
msgstr "Период удаления"

#: models.py:96
msgid "Delete time unit"
msgstr "Единица измерения периода удаления"

#: models.py:104
msgid "Documents types"
msgstr "Типы документов"

#: models.py:183
msgid ""
"UUID of a document, universally Unique ID. An unique identifiergenerated for"
" each document."
msgstr ""

#: models.py:193
msgid "The name of the document."
msgstr ""

#: models.py:197
msgid "An optional short text describing a document."
msgstr ""

#: models.py:198 search.py:22 search.py:45
msgid "Description"
msgstr "Описание"

#: models.py:202
msgid ""
"The server date and time when the document was finally processed and added "
"to the system."
msgstr ""

#: models.py:204 models.py:1058
msgid "Added"
msgstr "Добавлено"

#: models.py:208
msgid "The dominant language in the document."
msgstr ""

#: models.py:213
msgid "Whether or not this document is in the trash."
msgstr ""

#: models.py:214
msgid "In trash?"
msgstr "В корзине?"

#: models.py:219
msgid "The server date and time when the document was moved to the trash."
msgstr ""

#: models.py:221
msgid "Date and time trashed"
msgstr "Время и дата удаления"

#: models.py:225
msgid ""
"A document stub is a document with an entry on the database but no file "
"uploaded. This could be an interrupted upload or a deferred upload via the "
"API."
msgstr "Заглушка документа - это запись в базе данных без самого документа. Документ может оказатсья заглушкой, если его загрузка оборвалась, или выполняется его отложенная загрузка через API."

#: models.py:228
msgid "Is stub?"
msgstr "Является заглушкой?"

#: models.py:241
#, python-format
msgid "Document stub, id: %d"
msgstr "Заглушка документа, ид: %d"

#: models.py:431
msgid "The server date and time when the document version was processed."
msgstr ""

#: models.py:432
msgid "Timestamp"
msgstr "временная метка"

#: models.py:436
msgid "An optional short text describing the document version."
msgstr ""

#: models.py:437
msgid "Comment"
msgstr "Комментарий"

#: models.py:443
msgid "File"
msgstr "Файл"

#: models.py:447
msgid ""
"The document version's file mimetype. MIME types are a standard way to "
"describe the format of a file, in this case the file format of the document."
" Some examples: \"text/plain\" or \"image/jpeg\". "
msgstr ""

#: models.py:451 search.py:19 search.py:39
msgid "MIME type"
msgstr "MIME type"

#: models.py:455
msgid ""
"The document version file encoding. binary 7-bit, binary 8-bit, text, "
"base64, etc."
msgstr ""

#: models.py:457
msgid "Encoding"
msgstr "Кодировка"

#: models.py:469 models.py:470 models.py:788
msgid "Document version"
msgstr "Версия документа"

#: models.py:774
msgid "Quick label"
msgstr "Быстрая метка"

#: models.py:792
msgid "Page number"
msgstr "Номер страницы"

#: models.py:799 models.py:1011 models.py:1044
msgid "Document page"
msgstr "Страница документа"

#: models.py:800 models.py:1045
msgid "Document pages"
msgstr "Страницы документа"

#: models.py:804
#, python-format
msgid "Page %(page_num)d out of %(total_pages)d of %(document)s"
msgstr "Страница %(page_num)d из %(total_pages)d  %(document)s"

#: models.py:1014
msgid "Date time"
msgstr "Дата и время"

#: models.py:1016
msgid "Filename"
msgstr "Имя файла"

#: models.py:1024
msgid "Document page cached image"
msgstr ""

#: models.py:1025
msgid "Document page cached images"
msgstr ""

#: models.py:1064
msgid "Duplicated document"
msgstr ""

#: models.py:1078 models.py:1107
msgid "User"
msgstr "Пользователь"

#: models.py:1088
msgid "Favorite document"
msgstr ""

#: models.py:1089
msgid "Favorite documents"
msgstr ""

#: models.py:1114
msgid "Accessed"
msgstr "Допущен"

#: models.py:1121
msgid "Recent document"
msgstr "Недавние документы"

#: models.py:1122
msgid "Recent documents"
msgstr "Последние документы"

#: permissions.py:10
msgid "Create documents"
msgstr "Создание документов"

#: permissions.py:13
msgid "Delete documents"
msgstr "Удаление документов"

#: permissions.py:16
msgid "Trash documents"
msgstr "Переместить документы в корзину"

#: permissions.py:19 views/document_views.py:502
msgid "Download documents"
msgstr "Загрузка документов"

#: permissions.py:22
msgid "Edit documents"
msgstr "Редактировать документы"

#: permissions.py:25
msgid "Create new document versions"
msgstr "Создать новые версии документа"

#: permissions.py:28
msgid "Edit document properties"
msgstr "Редактирование свойств документа"

#: permissions.py:31
msgid "Print documents"
msgstr "Печать документов"

#: permissions.py:34
msgid "Restore trashed document"
msgstr ""

#: permissions.py:37
msgid "Execute document modifying tools"
msgstr "Выполнить изменения документа"

#: permissions.py:41
msgid "Revert documents to a previous version"
msgstr "Восстановить документы до предыдущей версии"

#: permissions.py:45
msgid "View documents' versions list"
msgstr ""

#: permissions.py:48
msgid "View documents"
msgstr "Просмотр документов"

#: permissions.py:58
msgid "Create document types"
msgstr "Создание типов документов"

#: permissions.py:61
msgid "Delete document types"
msgstr "Удалить типы документов"

#: permissions.py:64
msgid "Edit document types"
msgstr "Редактировать  типы  документов"

#: permissions.py:67
msgid "View document types"
msgstr "Просмотр типов документов"

#: queues.py:9
msgid "Converter"
msgstr "Конвертер"

#: queues.py:12
msgid "Documents periodic"
msgstr ""

#: queues.py:15
msgid "Uploads"
msgstr ""

#: queues.py:23
msgid "Generate document page image"
msgstr ""

#: queues.py:28
msgid "Delete a document"
msgstr ""

#: queues.py:32
msgid "Clean empty duplicate lists"
msgstr ""

#: queues.py:37
msgid "Check document type delete periods"
msgstr ""

#: queues.py:41
msgid "Check document type trash periods"
msgstr ""

#: queues.py:45
msgid "Delete document stubs"
msgstr ""

#: queues.py:50
msgid "Clear image cache"
msgstr ""

#: queues.py:55
msgid "Update document page count"
msgstr ""

#: queues.py:59
msgid "Upload new document version"
msgstr ""

#: settings.py:17
msgid ""
"Path to the Storage subclass to use when storing the cached document image "
"files."
msgstr ""

#: settings.py:26
msgid "Arguments to pass to the DOCUMENT_CACHE_STORAGE_BACKEND."
msgstr ""

#: settings.py:32
msgid ""
"Disables the first cache tier which stores high resolution, non transformed "
"versions of documents's pages."
msgstr ""

#: settings.py:39
msgid ""
"Disables the second cache tier which stores medium to low resolution, "
"transformed (rotated, zoomed, etc) versions of documents' pages."
msgstr ""

#: settings.py:53
msgid "Maximum number of favorite documents to remember per user."
msgstr ""

#: settings.py:59
msgid ""
"Detect the orientation of each of the document's pages and create a "
"corresponding rotation transformation to display it rightside up. This is an"
" experimental feature and it is disabled by default."
msgstr ""

#: settings.py:67
msgid "Default documents language (in ISO639-3 format)."
msgstr ""

#: settings.py:71
msgid "List of supported document languages. In ISO639-3 format."
msgstr ""

#: settings.py:76
msgid ""
"Time in seconds that the browser should cache the supplied document images. "
"The default of 31559626 seconds corresponde to 1 year."
msgstr ""

#: settings.py:95
msgid ""
"Maximum number of recently accessed (created, edited, viewed) documents to "
"remember per user."
msgstr ""

#: settings.py:102
msgid "Maximum number of recently created documents to show."
msgstr ""

#: settings.py:108
msgid "Amount in degrees to rotate a document page per user interaction."
msgstr "Градус поворота страницы документа в интерактивном режиме."

#: settings.py:114
msgid "Path to the Storage subclass to use when storing document files."
msgstr ""

#: settings.py:122
msgid "Arguments to pass to the DOCUMENT_STORAGE_BACKEND."
msgstr ""

#: settings.py:126
msgid "Height in pixels of the document thumbnail image."
msgstr ""

#: settings.py:137
msgid ""
"Maximum amount in percent (%) to allow user to zoom in a document page "
"interactively."
msgstr "Максимальный процент увеличения страницы документа пользователем."

#: settings.py:144
msgid ""
"Minimum amount in percent (%) to allow user to zoom out a document page "
"interactively."
msgstr "Процент уменьшения масштаба страницы документа в интерактивном режиме."

#: settings.py:151
msgid "Amount in percent zoom in or out a document page per user interaction."
msgstr "Процент увеличения страницы документа пользователем."

#: statistics.py:16
msgid "January"
msgstr ""

#: statistics.py:16
msgid "February"
msgstr ""

#: statistics.py:16
msgid "March"
msgstr ""

#: statistics.py:16
msgid "April"
msgstr ""

#: statistics.py:16
msgid "May"
msgstr ""

#: statistics.py:17
msgid "June"
msgstr ""

#: statistics.py:17
msgid "July"
msgstr ""

#: statistics.py:17
msgid "August"
msgstr ""

#: statistics.py:17
msgid "September"
msgstr ""

#: statistics.py:17
msgid "October"
msgstr ""

#: statistics.py:18
msgid "November"
msgstr ""

#: statistics.py:18
msgid "December"
msgstr ""

#: statistics.py:235
msgid "New documents per month"
msgstr "Новых документов в месяц"

#: statistics.py:242
msgid "New document versions per month"
msgstr "Новых версий документов в месяц"

#: statistics.py:249
msgid "New document pages per month"
msgstr "Новых страниц документов в месяц"

#: statistics.py:256
msgid "Total documents at each month"
msgstr "Всего документов в месяц"

#: statistics.py:263
msgid "Total document versions at each month"
msgstr "Новых версий документов в месяц"

#: statistics.py:270
msgid "Total document pages at each month"
msgstr "Всего страниц документов в месяц"

#: templates/documents/forms/widgets/document_page_carousel.html:16
#, python-format
msgid ""
"\n"
"                    Page %(page_number)s of %(total_pages)s\n"
"                "
msgstr ""

#: templates/documents/forms/widgets/document_page_carousel.html:22
msgid "No pages to display"
msgstr ""

#: views/document_page_views.py:49
#, python-format
msgid "Pages for document: %s"
msgstr "Страницы документа: %s"

#: views/document_page_views.py:104
msgid "Unknown view keyword argument schema, unable to redirect."
msgstr ""

#: views/document_page_views.py:136
msgid "There are no more pages in this document"
msgstr " Нет более страниц в этом документе"

#: views/document_page_views.py:153
msgid "You are already at the first page of this document"
msgstr "Вы уже на первой странице этого документа"

#: views/document_page_views.py:181
#, python-format
msgid "Image of: %s"
msgstr "Изображение для: %s"

#: views/document_type_views.py:46
#, python-format
msgid "Documents of type: %s"
msgstr "Документы с типом: %s"

#: views/document_type_views.py:64
msgid ""
"Document types are the most basic units of configuration. Everything in the "
"system will depend on them. Define a document type for each type of physical"
" document you intend to upload. Example document types: invoice, receipt, "
"manual, prescription, balance sheet."
msgstr ""

#: views/document_type_views.py:70
msgid "No document types available"
msgstr ""

#: views/document_type_views.py:102
msgid "All documents of this type will be deleted too."
msgstr "Все документы этого типа будут также удалены."

#: views/document_type_views.py:104
#, python-format
msgid "Delete the document type: %s?"
msgstr "Удалить тип документа: %s?"

#: views/document_type_views.py:120
#, python-format
msgid "Edit document type: %s"
msgstr "Редактировать тип документа: %s"

#: views/document_type_views.py:150
#, python-format
msgid "Create quick label for document type: %s"
msgstr "Создать быструю метку для типа документа: %s"

#: views/document_type_views.py:171
#, python-format
msgid "Edit quick label \"%(filename)s\" from document type \"%(document_type)s\""
msgstr "Редактировать быструю метку %(filename)s\" с типа документов \"%(document_type)s\""

#: views/document_type_views.py:196
#, python-format
msgid ""
"Delete the quick label: %(label)s, from document type \"%(document_type)s\"?"
msgstr "Снять быструю метку %(label)s с типа документа \"%(document_type)s\"?"

#: views/document_type_views.py:232
msgid ""
"Quick labels are predetermined filenames that allow the quick renaming of "
"documents as they are uploaded by selecting them from a list. Quick labels "
"can also be used after the documents have been uploaded."
msgstr ""

#: views/document_type_views.py:238
msgid "There are no quick labels for this document type"
msgstr ""

#: views/document_type_views.py:241
#, python-format
msgid "Quick labels for document type: %s"
msgstr "Быстрые метки для типа документа: %s"

#: views/document_version_views.py:48
#, python-format
msgid "Versions of document: %s"
msgstr "Версии документа: %s"

#: views/document_version_views.py:62
msgid "All later version after this one will be deleted too."
msgstr "Все более поздние версии после этого будут удалены"

#: views/document_version_views.py:65
msgid "Revert to this version?"
msgstr "Вернуться к этой версии?"

#: views/document_version_views.py:75
msgid "Document version reverted successfully"
msgstr "Версия документа восстановлена"

#: views/document_version_views.py:80
#, python-format
msgid "Error reverting document version; %s"
msgstr "Ошибка получения версии документа %s"

#: views/document_version_views.py:99
msgid "Download document version"
msgstr ""

#: views/document_version_views.py:165
#, python-format
msgid "Preview of document version: %s"
msgstr ""

#: views/document_views.py:71
#, python-format
msgid "Error retrieving document list: %(exception)s."
msgstr ""

#: views/document_views.py:91
msgid ""
"This could mean that no documents have been uploaded or that your user "
"account has not been granted the view permission for any document or "
"document type."
msgstr ""

#: views/document_views.py:95
msgid "No documents available"
msgstr ""

#: views/document_views.py:109
msgid "Delete the selected document?"
msgstr "Удалить выбранный документ?"

#: views/document_views.py:130
#, python-format
msgid "Document: %(document)s deleted."
msgstr "Документ: %(document)s удалён."

#: views/document_views.py:138
msgid "Delete the selected documents?"
msgstr "Удалить выбранные документы?"

#: views/document_views.py:161
msgid ""
"To avoid loss of data, documents are not deleted instantly. First, they are "
"placed in the trash can. From here they can be then finally deleted or "
"restored."
msgstr ""

#: views/document_views.py:166
msgid "There are no documents in the trash can"
msgstr ""

#: views/document_views.py:179
#, python-format
msgid "Document type change request performed on %(count)d document"
msgstr ""

#: views/document_views.py:182
#, python-format
msgid "Document type change request performed on %(count)d documents"
msgstr ""

#: views/document_views.py:189
msgid "Change"
msgstr ""

#: views/document_views.py:191
msgid "Change the type of the selected document"
msgid_plural "Change the type of the selected documents"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: views/document_views.py:202
#, python-format
msgid "Change the type of the document: %s"
msgstr ""

#: views/document_views.py:223
#, python-format
msgid "Document type for \"%s\" changed successfully."
msgstr "Тип документа для \"%s\" успешно изменён."

#: views/document_views.py:248
msgid "Only exact copies of this document will be shown in the this list."
msgstr ""

#: views/document_views.py:252
msgid "There are no duplicates for this document"
msgstr ""

#: views/document_views.py:255
#, python-format
msgid "Duplicates for document: %s"
msgstr ""

#: views/document_views.py:284
#, python-format
msgid "Edit properties of document: %s"
msgstr "Правка свойств документа: %s"

#: views/document_views.py:318
#, python-format
msgid "Preview of document: %s"
msgstr "Предпросмотр документа: %s"

#: views/document_views.py:324
msgid "Restore the selected document?"
msgstr "Восстановить выбранный документ?"

#: views/document_views.py:345
#, python-format
msgid "Document: %(document)s restored."
msgstr "Документ %(document)s восстановлен."

#: views/document_views.py:353
msgid "Restore the selected documents?"
msgstr "Восстановить выбранные документы?"

#: views/document_views.py:364
#, python-format
msgid "Move \"%s\" to the trash?"
msgstr "Перместить \"%s\" в корзину?"

#: views/document_views.py:387
#, python-format
msgid "Document: %(document)s moved to trash successfully."
msgstr "Документ %(document)s успешно перемещён в корзину."

#: views/document_views.py:400
msgid "Move the selected documents to the trash?"
msgstr "Переместить выделенные документы в корзину?"

#: views/document_views.py:418
#, python-format
msgid "Properties for document: %s"
msgstr "Свойства документа: %s"

#: views/document_views.py:424
msgid "Empty trash?"
msgstr "Очистить корзину?"

#: views/document_views.py:437
msgid "Trash emptied successfully"
msgstr "Корзина успешно очищена"

#: views/document_views.py:500
msgid "Download"
msgstr "Скачать"

#: views/document_views.py:606
#, python-format
msgid "%(count)d document queued for page count recalculation"
msgstr ""

#: views/document_views.py:609
#, python-format
msgid "%(count)d documents queued for page count recalculation"
msgstr ""

#: views/document_views.py:617
msgid "Recalculate the page count of the selected document?"
msgid_plural "Recalculate the page count of the selected documents?"
msgstr[0] "Пересчитать количество страниц для выбранного документа?"
msgstr[1] "Пересчитать количество страниц для выбранных документов?"
msgstr[2] "Пересчитать количество страниц для выбранных документов?"
msgstr[3] "Пересчитать количество страниц для выбранных документов?"

#: views/document_views.py:628
#, python-format
msgid "Recalculate the page count of the document: %s?"
msgstr ""

#: views/document_views.py:644
#, python-format
msgid ""
"Document \"%(document)s\" is empty. Upload at least one document version "
"before attempting to detect the page count."
msgstr ""

#: views/document_views.py:657
#, python-format
msgid "Transformation clear request processed for %(count)d document"
msgstr ""

#: views/document_views.py:660
#, python-format
msgid "Transformation clear request processed for %(count)d documents"
msgstr ""

#: views/document_views.py:668
msgid "Clear all the page transformations for the selected document?"
msgid_plural "Clear all the page transformations for the selected document?"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: views/document_views.py:679
#, python-format
msgid "Clear all the page transformations for the document: %s?"
msgstr ""

#: views/document_views.py:694 views/document_views.py:722
#, python-format
msgid ""
"Error deleting the page transformations for document: %(document)s; "
"%(error)s."
msgstr "Ошибка при удалении страницы для преобразования документов: %(document)s; %(error)s."

#: views/document_views.py:730
msgid "Transformations cloned successfully."
msgstr ""

#: views/document_views.py:745 views/document_views.py:818
msgid "Submit"
msgstr "Подтвердить"

#: views/document_views.py:747
#, python-format
msgid "Clone page transformations for document: %s"
msgstr ""

#: views/document_views.py:821
#, python-format
msgid "Print: %s"
msgstr "Печать: %s"

#: views/document_views.py:856
msgid ""
"Duplicates are documents that are composed of the exact same file, down to "
"the last byte. Files that have the same text or OCR but are not identical or"
" were saved using a different file format will not appear as duplicates."
msgstr ""

#: views/document_views.py:863
msgid "There are no duplicated documents"
msgstr ""

#: views/document_views.py:881
#, python-format
msgid ""
"Favorited documents will be listed in this view. Up to %(count)d documents "
"can be favorited per user. "
msgstr ""

#: views/document_views.py:884
msgid "There are no favorited documents."
msgstr ""

#: views/document_views.py:895
#, python-format
msgid "%(count)d document added to favorites."
msgstr ""

#: views/document_views.py:898
#, python-format
msgid "%(count)d documents added to favorites."
msgstr ""

#: views/document_views.py:905
msgid "Add"
msgstr "Добавить"

#: views/document_views.py:908
msgid "Add the selected document to favorites"
msgid_plural "Add the selected documents to favorites"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: views/document_views.py:921
#, python-format
msgid "Document \"%(instance)s\" is not in favorites."
msgstr ""

#: views/document_views.py:925
#, python-format
msgid "%(count)d document removed from favorites."
msgstr ""

#: views/document_views.py:928
#, python-format
msgid "%(count)d documents removed from favorites."
msgstr ""

#: views/document_views.py:935
msgid "Remove"
msgstr "Удалить"

#: views/document_views.py:938
msgid "Remove the selected document from favorites"
msgid_plural "Remove the selected documents from favorites"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: views/document_views.py:963
msgid ""
"This view will list the latest documents viewed or manipulated in any way by"
" this user account."
msgstr ""

#: views/document_views.py:967
msgid "There are no recently accessed document"
msgstr ""

#: views/document_views.py:987
msgid "This view will list the latest documents uploaded in the system."
msgstr ""

#: views/document_views.py:991
msgid "There are no recently added document"
msgstr ""

#: views/misc_views.py:18
msgid "Clear the document image cache?"
msgstr "Очистить кэш изображений документа?"

#: views/misc_views.py:25
msgid "Document cache clearing queued successfully."
msgstr "Очистка кэша документов успешно помещена в очередь."

#: views/misc_views.py:31
msgid "Scan for duplicated documents?"
msgstr ""

#: views/misc_views.py:38
msgid "Duplicated document scan queued successfully."
msgstr ""

#: widgets.py:81 widgets.py:85
#, python-format
msgid "Pages: %d"
msgstr ""
