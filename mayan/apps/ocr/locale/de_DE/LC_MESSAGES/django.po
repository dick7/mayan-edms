# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Berny <berny@bernhard-marx.de>, 2015-2016
# Bjoern Kowarsch <bjoern.kowarsch@gmail.com>, 2018
# Felix <felix.com@gmx.com>, 2018
# Mathias Behrle <mathiasb@m9s.biz>, 2018
# Mathias Behrle <mbehrle@m9s.biz>, 2014
# Stefan Lodders <sl@suchreflex.de>, 2012
# tilmannsittig <tilmann.sittig@web.de>, 2012
# Tobias Paepke <tobias.paepke@paepke.net>, 2014
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:32-0400\n"
"PO-Revision-Date: 2019-04-01 22:41+0000\n"
"Last-Translator: Mathias Behrle <mathiasb@m9s.biz>\n"
"Language-Team: German (Germany) (http://www.transifex.com/rosarior/mayan-edms/language/de_DE/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de_DE\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:73 apps.py:148 apps.py:152 events.py:7 links.py:19 links.py:24
#: permissions.py:7 queues.py:7 settings.py:7
msgid "OCR"
msgstr "OCR-Schrifterkennung"

#: apps.py:123
msgid "Document"
msgstr "Dokument"

#: apps.py:127
msgid "Added"
msgstr "Hinzugefügt"

#: apps.py:131 models.py:69
msgid "Result"
msgstr "Ergebnis"

#: events.py:10
msgid "Document version submitted for OCR"
msgstr "Dokumentenversion, die in die OCR-Warteschlange eingereiht ist"

#: events.py:14
msgid "Document version OCR finished"
msgstr "Dokumentenversion, für die die OCR-Schrifterkennung durchgeführt wurde"

#: forms.py:17 forms.py:48
msgid "Contents"
msgstr "Inhalte"

#: forms.py:77
#, python-format
msgid "Page %(page_number)d"
msgstr "Seite %(page_number)d"

#: links.py:29 links.py:32
msgid "Submit for OCR"
msgstr "In die OCR-Verarbeitung einstellen"

#: links.py:36
msgid "Setup OCR"
msgstr "OCR Einrichtung"

#: links.py:41
msgid "OCR documents per type"
msgstr "Texterkennung pro Dokumententyp durchführen"

#: links.py:46 links.py:50 views.py:144
msgid "OCR errors"
msgstr "OCR Fehler"

#: links.py:55
msgid "Download OCR text"
msgstr "OCR-Text herunterladen"

#: models.py:20
msgid "Document type"
msgstr "Dokumententyp"

#: models.py:24
msgid "Automatically queue newly created documents for OCR."
msgstr "Neue Dokumente automatisch in die OCR-Warteschlange einstellen."

#: models.py:30
msgid "Document type settings"
msgstr "Dokumententyp Einstellungen"

#: models.py:31
msgid "Document types settings"
msgstr "Dokumententypen Einstellungen"

#: models.py:42
msgid "Document page"
msgstr "Dokumentenseite"

#: models.py:46
msgid "The actual text content extracted by the OCR backend."
msgstr "Der Textinhalt, der von der OCR-Schrifterkennung erkannt wurde"

#: models.py:47
msgid "Content"
msgstr "Inhalt"

#: models.py:53
msgid "Document page OCR content"
msgstr "Von der OCR-Schrifterkennung erkannter Inhalt der Dokumentenseite"

#: models.py:54
msgid "Document pages OCR contents"
msgstr "Von der OCR-Schrifterkennung erkannte Inhalte von Dokumentenseiten"

#: models.py:64
msgid "Document version"
msgstr "Dokumentenversion"

#: models.py:67
msgid "Date time submitted"
msgstr "Einstellunsgzeit"

#: models.py:73
msgid "Document version OCR error"
msgstr "Fehler bei der OCR-Schrifterkennung für Dokumentenversion"

#: models.py:74
msgid "Document version OCR errors"
msgstr "Fehler bei der OCR-Schrifterkennung für Dokumentenversion"

#: permissions.py:10
msgid "Submit documents for OCR"
msgstr "Dokumente in die OCR-Warteschlange einstellen"

#: permissions.py:14
msgid "View the transcribed text from document"
msgstr "Verarbeiteten Text des Dokuments anzeigen"

#: permissions.py:18
msgid "Change document type OCR settings"
msgstr "OCR-Einstellungen für Dokumententyp beabeiten"

#: queues.py:9
msgid "Document version OCR"
msgstr "OCR-Schrifterkennung für Dokumentenversion"

#: settings.py:11
msgid "Full path to the backend to be used to do OCR."
msgstr "Vollständiger Pfad zum Backend, das für die OCR-Schrifterkennung verwendet werden soll."

#: settings.py:20
msgid "Set new document types to perform OCR automatically by default."
msgstr "Neue Dokumententypen definieren, für die die OCR-Texterkennung automatisch durchgeführt werden soll."

#: views.py:44
#, python-format
msgid "OCR result for document: %s"
msgstr "Ergebnis der OCR-Texterkennung für Dokument %s"

#: views.py:66
#, python-format
msgid "OCR result for document page: %s"
msgstr "Ergebnis der OCR-Texterkennung für Seite %s"

#: views.py:81
msgid "Submit the selected document to the OCR queue?"
msgid_plural "Submit the selected documents to the OCR queue?"
msgstr[0] "Senden Sie das ausgewählte Dokument an die OCR-Warteschlange."
msgstr[1] "Die ausgewählten Dokumente in die OCR-Warteschlange einreihen?"

#: views.py:95
msgid "Submit all documents of a type for OCR"
msgstr "Alle Dokumente eines Typs in die OCR-Verarbeitung einstellen"

#: views.py:107
#, python-format
msgid "%(count)d documents of type \"%(document_type)s\" added to the OCR queue."
msgstr "%(count)d Dokumente vom Typ \"%(document_type)s\" in OCR-Warteschlange eingereiht"

#: views.py:133
#, python-format
msgid "Edit OCR settings for document type: %s"
msgstr "OCR-Einstellungen für Dokumententyp %s bearbeiten"

#: views.py:162
#, python-format
msgid "OCR errors for document: %s"
msgstr "Fehler in der OCR-Schrifterkennung für Dokument: %s"
